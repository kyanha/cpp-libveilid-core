use crate::dart_isolate_wrapper::*;
use crate::ffi_types::*;
use crate::tools::*;
use cfg_if::*;
use data_encoding::BASE64URL_NOPAD;
use ffi_support::*;
use std::future::Future;
use lazy_static::*;
use opentelemetry::sdk::*;
use opentelemetry::*;
use opentelemetry_otlp::WithExportConfig;
use parking_lot::Mutex;
use serde::*;
use std::collections::BTreeMap;
use std::ffi::c_void;
use std::os::raw::c_char;
use std::sync::Arc;
use tracing::*;
use tracing_subscriber::prelude::*;
use tracing_subscriber::*;
use veilid_core::Encodable as _;

// Globals
lazy_static! {
    static ref CORE_INITIALIZED: Mutex<bool> = Mutex::new(false);
    static ref VEILID_API: AsyncMutex<Option<veilid_core::VeilidAPI>> = AsyncMutex::new(None);
    static ref FILTERS: Mutex<BTreeMap<&'static str, veilid_core::VeilidLayerFilter>> =
        Mutex::new(BTreeMap::new());
    static ref ROUTING_CONTEXTS: Mutex<BTreeMap<u32, veilid_core::RoutingContext>> =
        Mutex::new(BTreeMap::new());
    static ref TABLE_DBS: Mutex<BTreeMap<u32, veilid_core::TableDB>> =
        Mutex::new(BTreeMap::new());
    static ref TABLE_DB_TRANSACTIONS: Mutex<BTreeMap<u32, veilid_core::TableDBTransaction>> =
        Mutex::new(BTreeMap::new());
}

async fn get_veilid_api() -> veilid_core::VeilidAPIResult<veilid_core::VeilidAPI> {
    let api_lock = VEILID_API.lock().await;
    api_lock
        .as_ref()
        .cloned()
        .ok_or(veilid_core::VeilidAPIError::NotInitialized)
}

async fn take_veilid_api() -> veilid_core::VeilidAPIResult<veilid_core::VeilidAPI> {
    let mut api_lock = VEILID_API.lock().await;
    api_lock
        .take()
        .ok_or(veilid_core::VeilidAPIError::NotInitialized)
}

/////////////////////////////////////////
// FFI Helpers

// Declare external routine to release ffi strings
define_string_destructor!(free_string);

// Utility types for async API results
type APIResult<T> = veilid_core::VeilidAPIResult<T>;
const APIRESULT_VOID: APIResult<()> = APIResult::Ok(());

fn ffi_block_on<F: Future>(future: F) -> F::Output {
    let runtime = tokio::runtime::Builder::new_current_thread().enable_all().build().unwrap();
    runtime.block_on(future)
}

fn ffi_async<F: Future>(future: F)
where
    F: Future<Output = ()> + Send + 'static
{
    spawn(async move { future.await });
}

#[repr(transparent)]
#[derive(Debug)]
pub struct VeilidCallbackContext {
    pub data: *mut c_void,
}
unsafe impl Send for VeilidCallbackContext {}

// Callback types
pub type VeilidGenericCompletionCallback = unsafe extern "C" fn (VeilidResult, context: VeilidCallbackContext);
pub type VeilidCoreStartupCallback = VeilidGenericCompletionCallback;
pub type VeilidAttachCallback = VeilidGenericCompletionCallback;
pub type VeilidDetachCallback = VeilidGenericCompletionCallback;
pub type VeilidCoreShutdownCallback = VeilidGenericCompletionCallback;
pub type VeilidRoutingContextCallback = unsafe extern "C" fn (VeilidResult, context: VeilidCallbackContext, id: u32);
pub type VeilidRoutingContextAppCallCallback = unsafe extern "C" fn (VeilidResult, context: VeilidCallbackContext, answer: *const c_char);
pub type VeilidRoutingContextAppMessageCallback = VeilidGenericCompletionCallback;
pub type VeilidRoutingContextCreateDHTRecordCallback = unsafe extern "C" fn (VeilidResult, context: VeilidCallbackContext, descriptor: VeilidDHTRecordDescriptor);
pub type VeilidRoutingContextOpenDHTRecordCallback = unsafe extern "C" fn (VeilidResult, context: VeilidCallbackContext, descriptor: VeilidDHTRecordDescriptor);
pub type VeilidRoutingContextCloseDHTRecordCallback = VeilidGenericCompletionCallback;
pub type VeilidRoutingContextDeleteDHTRecordCallback = VeilidGenericCompletionCallback;
pub type VeilidRoutingContextGetDHTValueCallback = unsafe extern "C" fn (VeilidResult, context: VeilidCallbackContext, value_data: VeilidOptionalValueData);
pub type VeilidRoutingContextSetDHTValueCallback = unsafe extern "C" fn (VeilidResult, context: VeilidCallbackContext, value_data: VeilidOptionalValueData);
pub type VeilidRoutingContextWatchDHTValuesCallback = unsafe extern "C" fn (VeilidResult, context: VeilidCallbackContext, time_allowed: u64);
pub type VeilidRoutingContextCancelDHTWatchCallback = unsafe extern "C" fn (VeilidResult, context: VeilidCallbackContext, cancelled: bool);
pub type VeilidNewPrivateRouteCallback = unsafe extern "C" fn (VeilidResult, context: VeilidCallbackContext, blob: VeilidRouteBlob);
pub type VeilidNewCustomPrivateRouteCallback = unsafe extern "C" fn (VeilidResult, context: VeilidCallbackContext, blob: VeilidRouteBlob);
pub type VeilidImportRemotePrivateRouteCallback = unsafe extern "C" fn (VeilidResult, context: VeilidCallbackContext, route_id: VeilidCryptoKey);
pub type VeilidReleasePrivateRouteCallback = VeilidGenericCompletionCallback;
pub type VeilidAppCallReplyCallback = VeilidGenericCompletionCallback;
pub type VeilidOpenTableDbCallback = unsafe extern "C" fn (VeilidResult, context: VeilidCallbackContext, new_id: u32);
pub type VeilidDeleteTableDbCallback = unsafe extern "C" fn (VeilidResult, context: VeilidCallbackContext, deleted: bool);

/////////////////////////////////////////
// FFI-specific

#[derive(Debug, Deserialize, Serialize)]
pub struct VeilidFFIConfigLoggingTerminal {
    pub enabled: bool,
    pub level: veilid_core::VeilidConfigLogLevel,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct VeilidFFIConfigLoggingOtlp {
    pub enabled: bool,
    pub level: veilid_core::VeilidConfigLogLevel,
    pub grpc_endpoint: String,
    pub service_name: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct VeilidFFIConfigLoggingApi {
    pub enabled: bool,
    pub level: veilid_core::VeilidConfigLogLevel,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct VeilidFFIConfigLogging {
    pub terminal: VeilidFFIConfigLoggingTerminal,
    pub otlp: VeilidFFIConfigLoggingOtlp,
    pub api: VeilidFFIConfigLoggingApi,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct VeilidFFIConfig { 
    pub logging: VeilidFFIConfigLogging,
}

#[instrument]
fn initialize_veilid_panic_handler() {
    use std::sync::Once;
    static INIT_BACKTRACE: Once = Once::new();
    INIT_BACKTRACE.call_once(move || {
        std::env::set_var("RUST_BACKTRACE", "1");
        std::panic::set_hook(Box::new(move |panic_info| {

            // Try to log the trace to the terminal if we panicked before
            // initialize_veilid_core() was called successfully.
            let _ = tracing_subscriber::fmt().try_init();

            let (file, line) = if let Some(loc) = panic_info.location() {
                (loc.file(), loc.line())
            } else {
                ("<unknown>", 0)
            };
            error!("### Rust `panic!` hit at file '{}', line {}", file, line);
            if let Some(s) = panic_info.payload().downcast_ref::<&str>() {
                error!("panic payload: {:?}", s);
            } else if let Some(s) = panic_info.payload().downcast_ref::<String>() {
                error!("panic payload: {:?}", s);
            } else if let Some(a) = panic_info.payload().downcast_ref::<std::fmt::Arguments>() {
                error!("panic payload: {:?}", a);
            } else {
                error!("no panic payload");
            }
            error!(
                "  Complete stack trace:\n{:?}\n",
                backtrace::Backtrace::new()
            );

            // And stop the process, no recovery is going to be possible here
            std::process::abort();
        }));
    });
}

//////////////////////////////////////////////////////////////////////////////////
// C-compatible FFI Functions

#[no_mangle]
#[instrument]
pub extern "C" fn veilid_core_initialize(platform_config: FfiStr) {

    // Only do this once, ever
    // Until we have Dart native finalizers running on hot-restart, this will cause a crash if run more than once
    {
        let mut core_init = CORE_INITIALIZED.lock();
        if *core_init {
            return;
        }
        *core_init = true;
    }

    initialize_veilid_panic_handler();

    let platform_config = platform_config.into_opt_string();
    let platform_config: VeilidFFIConfig = veilid_core::deserialize_opt_json(platform_config)
        .expect("failed to deserialize plaform config json");

    // Set up subscriber and layers
    let subscriber = Registry::default();
    let mut layers = Vec::new();
    let mut filters = (*FILTERS).lock();

    // Terminal logger
    if platform_config.logging.terminal.enabled {
        let filter =
            veilid_core::VeilidLayerFilter::new(platform_config.logging.terminal.level, None);
        let layer = fmt::Layer::new()
            .compact()
            .with_writer(std::io::stdout)
            .with_filter(filter.clone());
        filters.insert("terminal", filter);
        layers.push(layer.boxed());
    };

    // OpenTelemetry logger
    if platform_config.logging.otlp.enabled {
        let grpc_endpoint = platform_config.logging.otlp.grpc_endpoint.clone();

        cfg_if! {
            if #[cfg(feature="rt-async-std")] {
                let exporter = opentelemetry_otlp::new_exporter()
                    .grpcio()
                    .with_endpoint(grpc_endpoint);
                let batch = opentelemetry::runtime::AsyncStd;
            } else if #[cfg(feature="rt-tokio")] {
                let exporter = opentelemetry_otlp::new_exporter()
                    .tonic()
                    .with_endpoint(format!("http://{}", grpc_endpoint));
                let batch = opentelemetry::runtime::Tokio;
            }
        }

        let tracer =
            opentelemetry_otlp::new_pipeline()
                .tracing()
                .with_exporter(exporter)
                .with_trace_config(opentelemetry::sdk::trace::config().with_resource(
                    Resource::new(vec![KeyValue::new(
                        opentelemetry_semantic_conventions::resource::SERVICE_NAME,
                        format!(
                        "{}:{}",
                        platform_config.logging.otlp.service_name,
                        hostname::get()
                            .map(|s| s.to_string_lossy().into_owned())
                            .unwrap_or_else(|_| "unknown".to_owned())),
                    )]),
                ))
                .install_batch(batch)
                .map_err(|e| format!("failed to install OpenTelemetry tracer: {}", e))
                .unwrap();

        let filter = veilid_core::VeilidLayerFilter::new(platform_config.logging.otlp.level, None);
        let layer = tracing_opentelemetry::layer()
            .with_tracer(tracer)
            .with_filter(filter.clone());
        filters.insert("otlp", filter);
        layers.push(layer.boxed());
    }

    // API logger
    if platform_config.logging.api.enabled {
        let filter = veilid_core::VeilidLayerFilter::new(platform_config.logging.api.level, None);
        let layer = veilid_core::ApiTracingLayer::get().with_filter(filter.clone());
        filters.insert("api", filter);
        layers.push(layer.boxed());
    }

    let subscriber = subscriber.with(layers);

    subscriber
        .try_init()
        .map_err(|e| format!("failed to initialize logging: {}", e))
        .expect("failed to initalize ffi platform");
}

/// TODO: convert layer into an enum.
#[no_mangle]
pub extern "C" fn veilid_change_log_level(layer: FfiStr, log_level: VeilidConfigLogLevel) {
    // get layer to change level on
    let layer = layer.into_opt_string().unwrap_or("all".to_owned());
    let layer = if layer == "all" { "".to_owned() } else { layer };

    // get log level to change layer to
    let internal_log_level: veilid_core::VeilidConfigLogLevel = match log_level {
        VeilidConfigLogLevel::Off => veilid_core::VeilidConfigLogLevel::Off,
        VeilidConfigLogLevel::Error => veilid_core::VeilidConfigLogLevel::Error,
        VeilidConfigLogLevel::Warn => veilid_core::VeilidConfigLogLevel::Warn,
        VeilidConfigLogLevel::Info => veilid_core::VeilidConfigLogLevel::Info,
        VeilidConfigLogLevel::Debug => veilid_core::VeilidConfigLogLevel::Debug,
        VeilidConfigLogLevel::Trace => veilid_core::VeilidConfigLogLevel::Trace
    };

    // change log level on appropriate layer
    let filters = (*FILTERS).lock();
    if layer.is_empty() {
        // Change all layers
        for f in filters.values() {
            f.set_max_level(internal_log_level);
        }
    } else {
        // Change a specific layer
        let f = filters.get(layer.as_str()).unwrap();
        f.set_max_level(internal_log_level);
    }
}

#[repr(C)]
pub enum VeilidResult {
    Ok,
    NotInitialized,
    AlreadyInitialized,
    Timeout,
    TryAgain,
    Shutdown,
    InvalidTarget,
    NoConnection,
    KeyNotFound,
    Internal,
    Unimplemented,
    ParseError,
    InvalidArgument,
    MissingArgument,
    Generic
}

fn to_veilid_result(api_result : Result<(), veilid_core::VeilidAPIError>) -> VeilidResult{
    let veilid_result = match api_result {
        Ok(_) => VeilidResult::Ok,
        Err(error) => match error {
            veilid_core::VeilidAPIError::NotInitialized => VeilidResult::NotInitialized,
            veilid_core::VeilidAPIError::AlreadyInitialized => VeilidResult::AlreadyInitialized,
            veilid_core::VeilidAPIError::Timeout => VeilidResult::Timeout,
            veilid_core::VeilidAPIError::TryAgain => VeilidResult::TryAgain,
            veilid_core::VeilidAPIError::Shutdown => VeilidResult::Shutdown,
            veilid_core::VeilidAPIError::InvalidTarget => VeilidResult::InvalidTarget,
            veilid_core::VeilidAPIError::NoConnection { .. } => VeilidResult::NoConnection,
            veilid_core::VeilidAPIError::KeyNotFound { .. } => VeilidResult::KeyNotFound,
            veilid_core::VeilidAPIError::Internal { .. } => VeilidResult::Internal,
            veilid_core::VeilidAPIError::Unimplemented { .. } => VeilidResult::Unimplemented,
            veilid_core::VeilidAPIError::ParseError { .. } => VeilidResult::ParseError,
            veilid_core::VeilidAPIError::InvalidArgument { .. } => VeilidResult::InvalidArgument,
            veilid_core::VeilidAPIError::MissingArgument { .. } => VeilidResult::MissingArgument,
            veilid_core::VeilidAPIError::Generic { .. } => VeilidResult::Generic
        }
    };
    veilid_result
}

#[no_mangle]
#[instrument]
pub extern "C" fn veilid_core_startup(completion_callback: VeilidCoreStartupCallback, context: VeilidCallbackContext, config: FfiStr, update_callback: unsafe extern "C" fn (*const c_char)) {
    let config = config.into_opt_string();

    let config_json = match config {
        Some(v) => v,
        None => {
            unsafe { completion_callback(VeilidResult::MissingArgument, context); }
            return;
        }
    };

    ffi_async(async move {
        let mut api_lock = VEILID_API.lock().await;
        if api_lock.is_some() {
            unsafe { completion_callback(VeilidResult::AlreadyInitialized, context); }
            return;
        }
    
        let update_callback = Arc::new(move |update: veilid_core::VeilidUpdate| {
            let update_message = veilid_core::serialize_json(update);
            unsafe { update_callback(rust_string_to_c(update_message)); }
        });

        let result = veilid_core::api_startup_json(update_callback, config_json).await;
        match result {
            Ok(veilid_api) => {
                *api_lock = Some(veilid_api);
                unsafe { completion_callback(VeilidResult::Ok, context); }
            },
            Err(error) => {
                unsafe { completion_callback(to_veilid_result(Err(error)), context); }
            }
        }
    });
}

/// Not implemented
#[no_mangle]
pub extern "C" fn veilid_get_state() -> VeilidResult {
    // let veilid_api = get_veilid_api().await?;
    // let core_state = veilid_api.get_state().await?;
    // APIResult::Ok(core_state)
    VeilidResult::Unimplemented
}

#[no_mangle]
pub extern "C" fn veilid_attach(completion_callback: VeilidAttachCallback, context: VeilidCallbackContext) {
    ffi_async(async move {
        let op = async {
            let veilid_api = get_veilid_api().await?;
            veilid_api.attach().await?;
            Ok(())
        };
        let result = to_veilid_result(op.await);
        unsafe { completion_callback(result, context); }
    })
}

#[no_mangle]
pub extern "C" fn veilid_detach(completion_callback: VeilidDetachCallback, context: VeilidCallbackContext) {
    ffi_async(async move {
        let op = async {
            let veilid_api = get_veilid_api().await?;
            veilid_api.detach().await?;
            Ok(())
        };
        let result = to_veilid_result(op.await);
        unsafe { completion_callback(result, context); }
    })
}

#[no_mangle]
#[instrument]
pub extern "C" fn veilid_core_shutdown(completion_callback: VeilidCoreShutdownCallback, context: VeilidCallbackContext) {
    ffi_async(async move {
        let op = async {
            let veilid_api = take_veilid_api().await?;
            veilid_api.shutdown().await;
            Ok(())
        };
        let result = to_veilid_result(op.await);
        unsafe { completion_callback(result, context); }
    })
}

fn add_routing_context(rc: &mut BTreeMap<u32, veilid_core::RoutingContext>, routing_context: veilid_core::RoutingContext) -> u32 {
    let mut next_id: u32 = 1;
    while rc.contains_key(&next_id) {
        next_id += 1;
    }
    rc.insert(next_id, routing_context);
    next_id
}

#[no_mangle]
pub extern "C" fn veilid_routing_context(completion_callback: VeilidRoutingContextCallback, context: VeilidCallbackContext) {
    ffi_async(async move {
        let mut new_id = 0;
        let op = async {
            let veilid_api = get_veilid_api().await?;
            let routing_context = veilid_api.routing_context();
            let mut rc = ROUTING_CONTEXTS.lock();
            new_id = add_routing_context(&mut *rc, routing_context);
            Ok(())
        };
        let result = to_veilid_result(op.await);
        unsafe { completion_callback(result, context, new_id); }
    })
}

#[no_mangle]
pub extern "C" fn veilid_release_routing_context(id: u32) -> bool {
    let mut rc = ROUTING_CONTEXTS.lock();
    if rc.remove(&id).is_none() {
        return false;
    }
    return true;
}

#[no_mangle]
pub extern "C" fn veilid_routing_context_with_privacy(id: u32) -> u32 {
    let mut rc = ROUTING_CONTEXTS.lock();
    let Some(routing_context) = rc.get(&id) else {
        return 0;
    };
    let Ok(routing_context) = routing_context.clone().with_privacy() else {
        return 0;
    };
    let new_id = add_routing_context(&mut rc, routing_context);
    new_id
}

#[no_mangle]
pub extern "C" fn veilid_routing_context_with_custom_privacy(id: u32, safety_selection: FfiStr) -> u32 {
    let safety_selection: veilid_core::SafetySelection =
        veilid_core::deserialize_opt_json(safety_selection.into_opt_string()).unwrap();

    let mut rc = ROUTING_CONTEXTS.lock();
    let Some(routing_context) = rc.get(&id) else {
        return 0;
    };
    let Ok(routing_context) = routing_context.clone().with_custom_privacy(safety_selection) else {
        return 0;
    };
    let new_id = add_routing_context(&mut rc, routing_context);
    new_id
}

#[no_mangle]
pub extern "C" fn veilid_routing_context_with_sequencing(id: u32, sequencing: FfiStr) -> u32 {
    let sequencing: veilid_core::Sequencing =
        veilid_core::deserialize_opt_json(sequencing.into_opt_string()).unwrap();

    let mut rc = ROUTING_CONTEXTS.lock();
    let Some(routing_context) = rc.get(&id) else {
        return 0;
    };
    let routing_context = routing_context.clone().with_sequencing(sequencing);
    let new_id = add_routing_context(&mut rc, routing_context);
    new_id
}

#[no_mangle]
pub extern "C" fn veilid_routing_context_app_call(completion_callback: VeilidRoutingContextAppCallCallback, context: VeilidCallbackContext, id: u32, target: FfiStr, request: FfiStr) {
    let target_string: String = target.into_opt_string().unwrap();
    let request: Vec<u8> = data_encoding::BASE64URL_NOPAD
        .decode(
            request.into_opt_string()
                .unwrap()
                .as_bytes(),
        )
        .unwrap();

    ffi_async(async move {
        let mut answer = String::new();

        let op = async {
            let routing_context = {
                let rc = ROUTING_CONTEXTS.lock();
                let Some(routing_context) = rc.get(&id) else {
                    return APIResult::Err(veilid_core::VeilidAPIError::invalid_argument("routing_context_app_call", "id", id));
                };
                routing_context.clone()
            };
            
            let veilid_api = get_veilid_api().await?;
            let target = veilid_api.parse_as_target(target_string).await?;
            let answer_raw = routing_context.app_call(target, request).await?;
            answer = data_encoding::BASE64URL_NOPAD.encode(&answer_raw);
            Ok(())
        };

        let result = to_veilid_result(op.await);
        unsafe { completion_callback(result, context, rust_string_to_c(answer)); }
    })
}

#[no_mangle]
pub extern "C" fn veilid_routing_context_app_message(completion_callback: VeilidRoutingContextAppMessageCallback, context: VeilidCallbackContext, id: u32, target: FfiStr, message: FfiStr) {
    let target_string: String = target.into_opt_string().unwrap();
    let message: Vec<u8> = data_encoding::BASE64URL_NOPAD
        .decode(
            message.into_opt_string()
                .unwrap()
                .as_bytes(),
        )
        .unwrap();
    ffi_async(async move {        
        let op = async {
            let routing_context = {
                let rc = ROUTING_CONTEXTS.lock();
                let Some(routing_context) = rc.get(&id) else {
                    return APIResult::Err(veilid_core::VeilidAPIError::invalid_argument("routing_context_app_message", "id", id));
                };
                routing_context.clone()
            };
            
            let veilid_api = get_veilid_api().await?;
            let target = veilid_api.parse_as_target(target_string).await?;
            routing_context.app_message(target, message).await?;
            Ok(())
        };

        let result = to_veilid_result(op.await);
        unsafe { completion_callback(result, context); }
    })
}

#[no_mangle]
pub extern "C" fn veilid_routing_context_create_dht_record(completion_callback: VeilidRoutingContextCreateDHTRecordCallback, context: VeilidCallbackContext, id: u32, schema: FfiStr, kind: u32) {
    let crypto_kind = if kind == 0 {
        None
    } else {
        Some(veilid_core::FourCC::from(kind))
    };
    let schema: veilid_core::DHTSchema = veilid_core::deserialize_opt_json(schema.into_opt_string()).unwrap();
    let mut descriptor = VeilidDHTRecordDescriptor::empty();

    ffi_async(async move {
        let op = async {
            let routing_context = {
                let rc = ROUTING_CONTEXTS.lock();
                let Some(routing_context) = rc.get(&id) else {
                    return APIResult::Err(veilid_core::VeilidAPIError::invalid_argument("routing_context_create_dht_record", "id", id));
                };
                routing_context.clone()
            };
            let dht_record_descriptor = routing_context.create_dht_record(schema, crypto_kind).await?;
            descriptor = VeilidDHTRecordDescriptor::new(dht_record_descriptor);
            Ok(())
        };

        let result = to_veilid_result(op.await);
        unsafe { completion_callback(result, context, descriptor ); }
    })
}

#[no_mangle]
pub extern "C" fn veilid_routing_context_open_dht_record(completion_callback: VeilidRoutingContextOpenDHTRecordCallback, context: VeilidCallbackContext, id: u32, key: FfiStr, writer: FfiStr) {
    let key: veilid_core::TypedKey = veilid_core::deserialize_opt_json(key.into_opt_string()).unwrap();
    let writer: Option<veilid_core::KeyPair> = writer.into_opt_string().map(|s| veilid_core::deserialize_json(&s).unwrap());
    let mut descriptor = VeilidDHTRecordDescriptor::empty();

    ffi_async(async move {
        let op = async {
            let routing_context = {
                let rc = ROUTING_CONTEXTS.lock();
                let Some(routing_context) = rc.get(&id) else {
                    return APIResult::Err(veilid_core::VeilidAPIError::invalid_argument("routing_context_open_dht_record", "id", id));
                };
                routing_context.clone()
            };
            let dht_record_descriptor = routing_context.open_dht_record(key, writer).await?;
            descriptor = VeilidDHTRecordDescriptor::new(dht_record_descriptor);
            Ok(())
        };

        let result = to_veilid_result(op.await);
        unsafe { completion_callback(result, context, descriptor); }
    })
}

#[no_mangle]
pub extern "C" fn veilid_routing_context_close_dht_record(completion_callback: VeilidRoutingContextCloseDHTRecordCallback, context: VeilidCallbackContext, id: u32, key: FfiStr) {
    let key: veilid_core::TypedKey = veilid_core::deserialize_opt_json(key.into_opt_string()).unwrap();

    ffi_async(async move {
        let op = async {
            let routing_context = {
                let rc = ROUTING_CONTEXTS.lock();
                let Some(routing_context) = rc.get(&id) else {
                    return APIResult::Err(veilid_core::VeilidAPIError::invalid_argument("routing_context_close_dht_record", "id", id));
                };
                routing_context.clone()
            };
            routing_context.close_dht_record(key).await?;
            Ok(())
        };

        let result = to_veilid_result(op.await);
        unsafe { completion_callback(result, context); }
    })
}

#[no_mangle]
pub extern "C" fn veilid_routing_context_delete_dht_record(completion_callback: VeilidRoutingContextDeleteDHTRecordCallback, context: VeilidCallbackContext, id: u32, key: FfiStr) {
    let key: veilid_core::TypedKey = veilid_core::deserialize_opt_json(key.into_opt_string()).unwrap();

    ffi_async(async move {
        let op = async {
            let routing_context = {
                let rc = ROUTING_CONTEXTS.lock();
                let Some(routing_context) = rc.get(&id) else {
                    return APIResult::Err(veilid_core::VeilidAPIError::invalid_argument("routing_context_delete_dht_record", "id", id));
                };
                routing_context.clone()
            };
            routing_context.delete_dht_record(key).await?;
            Ok(())
        };

        let result = to_veilid_result(op.await);
        unsafe { completion_callback(result, context); }
    })
}

#[no_mangle]
pub extern "C" fn veilid_routing_context_get_dht_value(completion_callback: VeilidRoutingContextGetDHTValueCallback, context: VeilidCallbackContext, id: u32, key: FfiStr, subkey: u32, force_refresh: bool) {
    let key: veilid_core::TypedKey = veilid_core::deserialize_opt_json(key.into_opt_string()).unwrap();
    let mut value_data = VeilidOptionalValueData::empty();

    ffi_async(async move {
        let op = async {
            let routing_context = {
                let rc = ROUTING_CONTEXTS.lock();
                let Some(routing_context) = rc.get(&id) else {
                    return APIResult::Err(veilid_core::VeilidAPIError::invalid_argument("routing_context_get_dht_value", "id", id));
                };
                routing_context.clone()
            };

            let res = routing_context.get_dht_value(key, subkey, force_refresh).await?;
            value_data = VeilidOptionalValueData::new(res.as_ref());
            Ok(())
        };

        let result = to_veilid_result(op.await);
        unsafe { completion_callback(result, context, value_data); }
    })
}

#[no_mangle]
pub extern "C" fn veilid_routing_context_set_dht_value(completion_callback: VeilidRoutingContextSetDHTValueCallback, context: VeilidCallbackContext, id: u32, key: FfiStr, subkey: u32, data: FfiStr) {
    let key: veilid_core::TypedKey = veilid_core::deserialize_opt_json(key.into_opt_string()).unwrap();
    let data: Vec<u8> = data_encoding::BASE64URL_NOPAD
    .decode(
        data.into_opt_string()
            .unwrap()
            .as_bytes(),
    )
    .unwrap();
    let mut value_data = VeilidOptionalValueData::empty();

    ffi_async(async move {
        let op = async {
            let routing_context = {
                let rc = ROUTING_CONTEXTS.lock();
                let Some(routing_context) = rc.get(&id) else {
                    return APIResult::Err(veilid_core::VeilidAPIError::invalid_argument("routing_context_set_dht_value", "id", id));
                };
                routing_context.clone()
            };

            let res = routing_context.set_dht_value(key, subkey, data).await?;
            value_data = VeilidOptionalValueData::new(res.as_ref());
            Ok(())
        };

        let result = to_veilid_result(op.await);
        unsafe { completion_callback(result, context, value_data); }
    })
}

#[no_mangle]
pub extern "C" fn veilid_routing_context_watch_dht_values(completion_callback: VeilidRoutingContextWatchDHTValuesCallback, context: VeilidCallbackContext, id: u32, key: FfiStr, subkeys: FfiStr, expiration: u64, count: u32) {
    let key: veilid_core::TypedKey = veilid_core::deserialize_opt_json(key.into_opt_string()).unwrap();
    let subkeys: veilid_core::ValueSubkeyRangeSet = veilid_core::deserialize_opt_json(subkeys.into_opt_string()).unwrap();
    let expiration = veilid_core::Timestamp::from(expiration);
    let mut time_allowed: u64 = 0;

    ffi_async(async move {
        let op = async {
            let routing_context = {
                let rc = ROUTING_CONTEXTS.lock();
                let Some(routing_context) = rc.get(&id) else {
                    return APIResult::Err(veilid_core::VeilidAPIError::invalid_argument("routing_context_watch_dht_values", "id", id));
                };
                routing_context.clone()
            };
            let res = routing_context.watch_dht_values(key, subkeys, expiration, count).await?;
            time_allowed = res.as_u64();
            Ok(())
        };

        let result = to_veilid_result(op.await);
        unsafe { completion_callback(result, context, time_allowed); }
    })
}

#[no_mangle]
pub extern "C" fn veilid_routing_context_cancel_dht_watch(completion_callback: VeilidRoutingContextCancelDHTWatchCallback, context: VeilidCallbackContext, id: u32, key: FfiStr, subkeys: FfiStr) {
    let key: veilid_core::TypedKey = veilid_core::deserialize_opt_json(key.into_opt_string()).unwrap();
    let subkeys: veilid_core::ValueSubkeyRangeSet = veilid_core::deserialize_opt_json(subkeys.into_opt_string()).unwrap();
    let mut cancelled = false;

    ffi_async(async move {
        let op = async {
            let routing_context = {
                let rc = ROUTING_CONTEXTS.lock();
                let Some(routing_context) = rc.get(&id) else {
                    return APIResult::Err(veilid_core::VeilidAPIError::invalid_argument("routing_context_set_dht_value", "id", id));
                };
                routing_context.clone()
            };
            cancelled = routing_context.cancel_dht_watch(key, subkeys).await?;
            Ok(())
        };

        let result = to_veilid_result(op.await);
        unsafe { completion_callback(result, context, cancelled); }
    })
}

#[no_mangle]
pub extern "C" fn veilid_new_private_route(completion_callback: VeilidNewPrivateRouteCallback, context: VeilidCallbackContext) {
    let mut route_blob = VeilidRouteBlob::empty();
    ffi_async(async move {
        let op = async {
            let veilid_api = get_veilid_api().await?;
            let (route_id, blob) = veilid_api.new_private_route().await?;
            route_blob = VeilidRouteBlob::new(route_id, blob);
            APIRESULT_VOID
        };

        let result = to_veilid_result(op.await);
        unsafe { completion_callback(result, context, route_blob); }
    })
}

#[no_mangle]
pub extern "C" fn veilid_new_custom_private_route(completion_callback: VeilidNewCustomPrivateRouteCallback, context: VeilidCallbackContext, stability: FfiStr, sequencing: FfiStr) {
    let stability: veilid_core::Stability = veilid_core::deserialize_opt_json(stability.into_opt_string()).unwrap();
    let sequencing: veilid_core::Sequencing = veilid_core::deserialize_opt_json(sequencing.into_opt_string()).unwrap();
    let mut route_blob = VeilidRouteBlob::empty();

    ffi_async(async move {
        let op = async {
            let veilid_api = get_veilid_api().await?;
            let (route_id, blob) = veilid_api
                .new_custom_private_route(&veilid_core::VALID_CRYPTO_KINDS, stability, sequencing)
                .await?;
            route_blob = VeilidRouteBlob::new(route_id, blob);
            APIRESULT_VOID
        };

        let result = to_veilid_result(op.await);
        unsafe { completion_callback(result, context, route_blob); }
    });
}

#[no_mangle]
pub extern "C" fn veilid_import_remote_private_route(completion_callback: VeilidImportRemotePrivateRouteCallback, context: VeilidCallbackContext, blob: FfiStr) {
    let blob: Vec<u8> = data_encoding::BASE64URL_NOPAD
        .decode(
            veilid_core::deserialize_opt_json::<String>(blob.into_opt_string())
                .unwrap()
                .as_bytes(),
        )
        .unwrap();

    let mut route_id = VeilidCryptoKey::empty();

    ffi_async(async move {
        let op = async {
            let veilid_api = get_veilid_api().await?;
            route_id = VeilidCryptoKey::new(veilid_api.import_remote_private_route(blob)?);
            APIRESULT_VOID
        };

        let result = to_veilid_result(op.await);
        unsafe { completion_callback(result, context, route_id); }
    });
}

#[no_mangle]
pub extern "C" fn veilid_release_private_route(completion_callback: VeilidReleasePrivateRouteCallback, context: VeilidCallbackContext, route_id: FfiStr) {
    let route_id: veilid_core::RouteId = veilid_core::deserialize_opt_json(route_id.into_opt_string()).unwrap();

    ffi_async(async move {
        let op = async {
            let veilid_api = get_veilid_api().await?;
            veilid_api.release_private_route(route_id)?;
            APIRESULT_VOID
        };

        let result = to_veilid_result(op.await);
        unsafe { completion_callback(result, context); }
    })
}

#[no_mangle]
pub extern "C" fn veilid_app_call_reply(completion_callback: VeilidAppCallReplyCallback, context: VeilidCallbackContext, call_id: FfiStr, message: FfiStr) {
    let call_id = call_id.into_opt_string().unwrap_or_default();
    let message = message.into_opt_string().unwrap_or_default();
    ffi_async(async move {
        let op = async {
            let call_id = match call_id.parse() {
                Ok(v) => v,
                Err(e) => {
                    return APIResult::Err(veilid_core::VeilidAPIError::invalid_argument(e, "call_id", call_id))
                }
            };
            let message = data_encoding::BASE64URL_NOPAD
                .decode(message.as_bytes())
                .map_err(|e| veilid_core::VeilidAPIError::invalid_argument(e, "message", message))?;
            let veilid_api = get_veilid_api().await?;
            veilid_api.app_call_reply(call_id, message).await?;
            APIRESULT_VOID
        };

        let result = to_veilid_result(op.await);
        unsafe { completion_callback(result, context); }
    })
}

fn add_table_db(table_db: veilid_core::TableDB) -> u32 {
    let mut next_id: u32 = 1;
    let mut rc = TABLE_DBS.lock();
    while rc.contains_key(&next_id) {
        next_id += 1;
    }
    rc.insert(next_id, table_db);
    next_id
}

#[no_mangle]
pub extern "C" fn veilid_open_table_db(completion_callback: VeilidOpenTableDbCallback, context: VeilidCallbackContext, name: FfiStr, column_count: u32) {
    let name = name.into_opt_string().unwrap_or_default();
    let mut new_id: u32 = 0;
    ffi_async(async move {
        let op = async {
            let veilid_api = get_veilid_api().await?;
            let tstore = veilid_api.table_store()?;
            let table_db = tstore.open(&name, column_count).await.map_err(veilid_core::VeilidAPIError::generic)?;
            new_id = add_table_db(table_db);
            APIRESULT_VOID
        };

        let result = to_veilid_result(op.await);
        unsafe { completion_callback(result, context, new_id); }
    })
}

#[no_mangle]
pub extern "C" fn veilid_release_table_db(id: u32) -> i32 {
    let mut rc = TABLE_DBS.lock();
    if rc.remove(&id).is_none() {
        return 0;
    }
    return 1;
}

#[no_mangle]
pub extern "C" fn veilid_delete_table_db(completion_callback: VeilidDeleteTableDbCallback, context: VeilidCallbackContext, name: FfiStr) {
    let name = name.into_opt_string().unwrap_or_default();
    let mut deleted = false;
    ffi_async(async move {
        let op = async {
            let veilid_api = get_veilid_api().await?;
            let tstore = veilid_api.table_store()?;
            deleted = tstore.delete(&name).await.map_err(veilid_core::VeilidAPIError::generic)?;
            APIRESULT_VOID
        };

        let result = to_veilid_result(op.await);
        unsafe { completion_callback(result, context, deleted); }
    })
}

#[no_mangle]
pub extern "C" fn veilid_table_db_get_column_count(id: u32) -> u32 {
    let table_dbs = TABLE_DBS.lock();
    let Some(table_db) = table_dbs.get(&id) else {
        return 0;
    };
    let Ok(cc) = table_db.clone().get_column_count() else {
        return 0;
    };
    return cc;
}

/// Not implemented.
#[no_mangle]
pub extern "C" fn veilid_table_db_get_keys(port: i64, id: u32, col: u32) {
    DartIsolateWrapper::new(port).spawn_result_json(async move {
        let table_db = {
            let table_dbs = TABLE_DBS.lock();
            let Some(table_db) = table_dbs.get(&id) else {
                return APIResult::Err(veilid_core::VeilidAPIError::invalid_argument("table_db_get_keys", "id", id));
            };
            table_db.clone()
        };

        let keys = table_db.get_keys(col).await?;
        let out: Vec<String> = keys.into_iter().map(|k| BASE64URL_NOPAD.encode(&k)).collect();
        APIResult::Ok(out)
    });
}

fn veilid_add_table_db_transaction(tdbt: veilid_core::TableDBTransaction) -> u32 {
    let mut next_id: u32 = 1;
    let mut tdbts = TABLE_DB_TRANSACTIONS.lock();
    while tdbts.contains_key(&next_id) {
        next_id += 1;
    }
    tdbts.insert(next_id, tdbt);
    next_id
}

#[no_mangle]
pub extern "C" fn veilid_table_db_transact(id: u32) -> u32 {
    let table_dbs = TABLE_DBS.lock();
    let Some(table_db) = table_dbs.get(&id) else {
        return 0;
    };
    let tdbt = table_db.clone().transact();
    let tdbtid = veilid_add_table_db_transaction(tdbt);
    return tdbtid;
}

#[no_mangle]
pub extern "C" fn veilid_release_table_db_transaction(id: u32) -> i32 {
    let mut tdbts = TABLE_DB_TRANSACTIONS.lock();
    if tdbts.remove(&id).is_none() {
        return 0;
    }
    return 1;
}

/// Not implemented.
#[no_mangle]
pub extern "C" fn veilid_table_db_transaction_commit(port: i64, id: u32) {
    DartIsolateWrapper::new(port).spawn_result(async move {
        let tdbt = {
            let tdbts = TABLE_DB_TRANSACTIONS.lock();
            let Some(tdbt) = tdbts.get(&id) else {
                return APIResult::Err(veilid_core::VeilidAPIError::invalid_argument("table_db_transaction_commit", "id", id));
            };
            tdbt.clone()
        };
        
        tdbt.commit().await?;
        APIRESULT_VOID
    });
}

/// Not implemented.
#[no_mangle]
pub extern "C" fn veilid_table_db_transaction_rollback(port: i64, id: u32) {
    DartIsolateWrapper::new(port).spawn_result(async move {
        let tdbt = {
            let tdbts = TABLE_DB_TRANSACTIONS.lock();
            let Some(tdbt) = tdbts.get(&id) else {
                return APIResult::Err(veilid_core::VeilidAPIError::invalid_argument("table_db_transaction_rollback", "id", id));
            };
            tdbt.clone()
        };
        
        tdbt.rollback();
        APIRESULT_VOID
    });
}

/// Not implemented.
#[no_mangle]
pub extern "C" fn veilid_table_db_transaction_store(port: i64, id: u32, col: u32, key: FfiStr, value: FfiStr) {
    let key: Vec<u8> = data_encoding::BASE64URL_NOPAD
        .decode(
            key.into_opt_string()
                .unwrap()
                .as_bytes(),
        )
        .unwrap();
    let value: Vec<u8> = data_encoding::BASE64URL_NOPAD
        .decode(
            value.into_opt_string()
                .unwrap()
                .as_bytes(),
        )
        .unwrap();
    DartIsolateWrapper::new(port).spawn_result(async move {
        let tdbt = {
            let tdbts = TABLE_DB_TRANSACTIONS.lock();
            let Some(tdbt) = tdbts.get(&id) else {
                return APIResult::Err(veilid_core::VeilidAPIError::invalid_argument("table_db_transaction_store", "id", id));
            };
            tdbt.clone()
        };
        
        tdbt.store(col, &key, &value)?;
        APIRESULT_VOID
    });
}

/// Not implemented.
#[no_mangle]
pub extern "C" fn veilid_table_db_transaction_delete(port: i64, id: u32, col: u32, key: FfiStr) {
    let key: Vec<u8> = data_encoding::BASE64URL_NOPAD
        .decode(
            key.into_opt_string()
                .unwrap()
                .as_bytes(),
        )
        .unwrap();
    DartIsolateWrapper::new(port).spawn_result(async move {
        let tdbt = {
            let tdbts = TABLE_DB_TRANSACTIONS.lock();
            let Some(tdbt) = tdbts.get(&id) else {
                return APIResult::Err(veilid_core::VeilidAPIError::invalid_argument("table_db_transaction_delete", "id", id));
            };
            tdbt.clone()
        };
        
        tdbt.delete(col, &key)?;
        APIRESULT_VOID
    });
}

/// Not implemented.
#[no_mangle]
pub extern "C" fn veilid_table_db_store(port: i64, id: u32, col: u32, key: FfiStr, value: FfiStr) {
    let key: Vec<u8> = data_encoding::BASE64URL_NOPAD
        .decode(
            key.into_opt_string()
                .unwrap()
                .as_bytes(),
        )
        .unwrap();
    let value: Vec<u8> = data_encoding::BASE64URL_NOPAD
        .decode(
            value.into_opt_string()
                .unwrap()
                .as_bytes(),
        )
        .unwrap();
    DartIsolateWrapper::new(port).spawn_result(async move {
        let table_db = {
            let table_dbs = TABLE_DBS.lock();
            let Some(table_db) = table_dbs.get(&id) else {
                return APIResult::Err(veilid_core::VeilidAPIError::invalid_argument("table_db_store", "id", id));
            };
            table_db.clone()
        };
        
        table_db.store(col, &key, &value).await?;
        APIRESULT_VOID
    });
}

/// Not implemented.
#[no_mangle]
pub extern "C" fn veilid_table_db_load(port: i64, id: u32, col: u32, key: FfiStr) {
    let key: Vec<u8> = data_encoding::BASE64URL_NOPAD
        .decode(key.into_opt_string()
                .unwrap()
                .as_bytes()
        )
        .unwrap();
    DartIsolateWrapper::new(port).spawn_result(async move {
        let table_db = {
            let table_dbs = TABLE_DBS.lock();
            let Some(table_db) = table_dbs.get(&id) else {
                return APIResult::Err(veilid_core::VeilidAPIError::invalid_argument("table_db_load", "id", id));
            };
            table_db.clone()
        };
        
        let out = table_db.load(col, &key).await?;
        let out = out.map(|x| data_encoding::BASE64URL_NOPAD.encode(&x));
        APIResult::Ok(out)
    });
}

/// Not implemented.
#[no_mangle]
pub extern "C" fn veilid_table_db_delete(port: i64, id: u32, col: u32, key: FfiStr) {
    let key: Vec<u8> = data_encoding::BASE64URL_NOPAD
        .decode(
            key.into_opt_string()
                .unwrap()
                .as_bytes(),
        )
        .unwrap();
    DartIsolateWrapper::new(port).spawn_result(async move {
        let table_db = {
            let table_dbs = TABLE_DBS.lock();
            let Some(table_db) = table_dbs.get(&id) else {
                return APIResult::Err(veilid_core::VeilidAPIError::invalid_argument("table_db_delete", "id", id));
            };
            table_db.clone()
        };
        
        let out = table_db.delete(col, &key).await?;
        let out = out.map(|x| data_encoding::BASE64URL_NOPAD.encode(&x));
        APIResult::Ok(out)
    });
}

#[no_mangle]
pub extern "C" fn veilid_valid_crypto_kinds() -> *mut c_char {
    veilid_core::serialize_json(veilid_core::VALID_CRYPTO_KINDS.iter().map(|k| (*k).into()).collect::<Vec<u32>>()).into_ffi_value()
}

#[no_mangle]
pub extern "C" fn veilid_best_crypto_kind() -> u32 {
    veilid_core::best_crypto_kind().into()
}

/// Not implemented.
#[no_mangle]
pub extern "C" fn veilid_verify_signatures(port: i64, node_ids: FfiStr, data: FfiStr, signatures: FfiStr) {
    let node_ids: Vec<veilid_core::TypedKey> =
        veilid_core::deserialize_opt_json(node_ids.into_opt_string()).unwrap();

    let data: Vec<u8> = data_encoding::BASE64URL_NOPAD
    .decode(
        data.into_opt_string()
            .unwrap()
            .as_bytes(),
    )
    .unwrap();

    let typed_signatures: Vec<veilid_core::TypedSignature> =
        veilid_core::deserialize_opt_json(signatures.into_opt_string()).unwrap();

    DartIsolateWrapper::new(port).spawn_result_json(async move {
        let veilid_api = get_veilid_api().await?;
        let crypto = veilid_api.crypto()?;
        let out = crypto.verify_signatures(&node_ids, &data, &typed_signatures)?;
        APIResult::Ok(out)
    });
}

/// Not implemented.
#[no_mangle]
pub extern "C" fn veilid_generate_signatures(port: i64, data: FfiStr, key_pairs: FfiStr) {

    let data: Vec<u8> = data_encoding::BASE64URL_NOPAD
    .decode(
        data.into_opt_string()
            .unwrap()
            .as_bytes(),
    )
    .unwrap();

    let key_pairs: Vec<veilid_core::TypedKeyPair> =
        veilid_core::deserialize_opt_json(key_pairs.into_opt_string()).unwrap();

    DartIsolateWrapper::new(port).spawn_result_json(async move {
        let veilid_api = get_veilid_api().await?;
        let crypto = veilid_api.crypto()?;
        let out = crypto.generate_signatures(&data, &key_pairs, |k, s| {
            veilid_core::TypedSignature::new(k.kind, s)
        })?;
        APIResult::Ok(out)
    });
}

/// Not implemented.
#[no_mangle]
pub extern "C" fn veilid_generate_key_pair(port: i64, kind: u32) {
    let kind: veilid_core::CryptoKind = veilid_core::FourCC::from(kind);

    DartIsolateWrapper::new(port).spawn_result_json(async move {
        let out = veilid_core::Crypto::generate_keypair(kind)?;
        APIResult::Ok(out)
    });
}

/// Not implemented.
#[no_mangle]
pub extern "C" fn veilid_crypto_cached_dh(port: i64, kind: u32, key: FfiStr, secret: FfiStr) {
    let kind: veilid_core::CryptoKind = veilid_core::FourCC::from(kind);

    let key: veilid_core::PublicKey =
        veilid_core::deserialize_opt_json(key.into_opt_string()).unwrap();
    let secret: veilid_core::SecretKey =
        veilid_core::deserialize_opt_json(secret.into_opt_string()).unwrap();
    
    DartIsolateWrapper::new(port).spawn_result_json(async move {
        let veilid_api = get_veilid_api().await?;
        let crypto = veilid_api.crypto()?;
        let csv = crypto.get(kind).ok_or_else(|| veilid_core::VeilidAPIError::invalid_argument("crypto_cached_dh", "kind", kind.to_string()))?;
        let out = csv.cached_dh(&key, &secret)?;
        APIResult::Ok(out)
    });
}

/// Not implemented.
#[no_mangle]
pub extern "C" fn veilid_crypto_compute_dh(port: i64, kind: u32, key: FfiStr, secret: FfiStr) {
    let kind: veilid_core::CryptoKind = veilid_core::FourCC::from(kind);
    
    let key: veilid_core::PublicKey =
        veilid_core::deserialize_opt_json(key.into_opt_string()).unwrap();
    let secret: veilid_core::SecretKey =
        veilid_core::deserialize_opt_json(secret.into_opt_string()).unwrap();

    DartIsolateWrapper::new(port).spawn_result_json(async move {
        let veilid_api = get_veilid_api().await?;
        let crypto = veilid_api.crypto()?;
        let csv = crypto.get(kind).ok_or_else(|| veilid_core::VeilidAPIError::invalid_argument("crypto_compute_dh", "kind", kind.to_string()))?;
        let out = csv.compute_dh(&key, &secret)?;
        APIResult::Ok(out)
    });
}

/// Not implemented.
#[no_mangle]
pub extern "C" fn veilid_crypto_random_bytes(port: i64, kind: u32, len: u32) {
    let kind: veilid_core::CryptoKind = veilid_core::FourCC::from(kind);

    DartIsolateWrapper::new(port).spawn_result(async move {
        let veilid_api = get_veilid_api().await?;
        let crypto = veilid_api.crypto()?;
        let csv = crypto.get(kind).ok_or_else(|| veilid_core::VeilidAPIError::invalid_argument("crypto_random_bytes", "kind", kind.to_string()))?;
        let out = csv.random_bytes(len);
        let out = data_encoding::BASE64URL_NOPAD.encode(&out);
        APIResult::Ok(out)
    });
}

/// Not implemented.
#[no_mangle]
pub extern "C" fn veilid_crypto_default_salt_length(port: i64, kind: u32) {
    let kind: veilid_core::CryptoKind = veilid_core::FourCC::from(kind);

    DartIsolateWrapper::new(port).spawn_result(async move {
        let veilid_api = get_veilid_api().await?;
        let crypto = veilid_api.crypto()?;
        let csv = crypto.get(kind).ok_or_else(|| veilid_core::VeilidAPIError::invalid_argument("crypto_default_salt_length", "kind", kind.to_string()))?;
        let out = csv.default_salt_length();
        APIResult::Ok(out)
    });
}

/// Not implemented.
#[no_mangle]
pub extern "C" fn veilid_crypto_hash_password(port: i64, kind: u32, password: FfiStr, salt: FfiStr ) {
    let kind: veilid_core::CryptoKind = veilid_core::FourCC::from(kind);
    let password: Vec<u8> = data_encoding::BASE64URL_NOPAD
        .decode(
            password.into_opt_string()
                .unwrap()
                .as_bytes(),
        )
        .unwrap();
    let salt: Vec<u8> = data_encoding::BASE64URL_NOPAD
        .decode(
            salt.into_opt_string()
                .unwrap()
                .as_bytes(),
        )
        .unwrap();

    DartIsolateWrapper::new(port).spawn_result(async move {
        let veilid_api = get_veilid_api().await?;
        let crypto = veilid_api.crypto()?;
        let csv = crypto.get(kind).ok_or_else(|| veilid_core::VeilidAPIError::invalid_argument("crypto_hash_password", "kind", kind.to_string()))?;
        let out = csv.hash_password(&password, &salt)?;
        APIResult::Ok(out)
    });
}

#[no_mangle]
pub extern "C" fn veilid_crypto_verify_password(kind: u32, password: FfiStr, password_hash: FfiStr, out_verified: *mut bool) -> VeilidResult {
    let kind: veilid_core::CryptoKind = veilid_core::FourCC::from(kind);
    let password: Vec<u8> = data_encoding::BASE64URL_NOPAD
        .decode(
            password.into_opt_string()
                .unwrap()
                .as_bytes(),
        )
        .unwrap();
    let password_hash = password_hash.into_opt_string().unwrap();

    to_veilid_result(ffi_block_on(async move {
        let veilid_api = get_veilid_api().await?;
        let crypto = veilid_api.crypto()?;
        let csv = crypto.get(kind).ok_or_else(|| veilid_core::VeilidAPIError::invalid_argument("crypto_verify_password", "kind", kind.to_string()))?;
        let out = csv.verify_password(&password, &password_hash)?;
        
        unsafe {
            *out_verified = out;
        }

        APIRESULT_VOID
    }))
}

/// Not implemented.
#[no_mangle]
pub extern "C" fn veilid_crypto_derive_shared_secret(port: i64, kind: u32, password: FfiStr, salt: FfiStr ) {
    let kind: veilid_core::CryptoKind = veilid_core::FourCC::from(kind);
    let password: Vec<u8> = data_encoding::BASE64URL_NOPAD
        .decode(
            password.into_opt_string()
                .unwrap()
                .as_bytes(),
        )
        .unwrap();
        let salt: Vec<u8> = data_encoding::BASE64URL_NOPAD
        .decode(
            salt.into_opt_string()
                .unwrap()
                .as_bytes(),
        )
        .unwrap();

    DartIsolateWrapper::new(port).spawn_result_json(async move {
        let veilid_api = get_veilid_api().await?;
        let crypto = veilid_api.crypto()?;
        let csv = crypto.get(kind).ok_or_else(|| veilid_core::VeilidAPIError::invalid_argument("crypto_derive_shared_secret", "kind", kind.to_string()))?;
        let out = csv.derive_shared_secret(&password, &salt)?;
        APIResult::Ok(out)
    });
}

/// Not implemented.
#[no_mangle]
pub extern "C" fn veilid_crypto_random_nonce(port: i64, kind: u32) {
    let kind: veilid_core::CryptoKind = veilid_core::FourCC::from(kind);

    DartIsolateWrapper::new(port).spawn_result_json(async move {
        let veilid_api = get_veilid_api().await?;
        let crypto = veilid_api.crypto()?;
        let csv = crypto.get(kind).ok_or_else(|| veilid_core::VeilidAPIError::invalid_argument("crypto_random_nonce", "kind", kind.to_string()))?;
        let out = csv.random_nonce();
        APIResult::Ok(out)
    });
}

/// Not implemented.
#[no_mangle]
pub extern "C" fn veilid_crypto_random_shared_secret(port: i64, kind: u32) {
    let kind: veilid_core::CryptoKind = veilid_core::FourCC::from(kind);

    DartIsolateWrapper::new(port).spawn_result_json(async move {
        let veilid_api = get_veilid_api().await?;
        let crypto = veilid_api.crypto()?;
        let csv = crypto.get(kind).ok_or_else(|| veilid_core::VeilidAPIError::invalid_argument("crypto_random_shared_secret", "kind", kind.to_string()))?;
        let out = csv.random_shared_secret();
        APIResult::Ok(out)
    });
}

/// Not implemented.
#[no_mangle]
pub extern "C" fn veilid_crypto_generate_key_pair(port: i64, kind: u32) {
    let kind: veilid_core::CryptoKind = veilid_core::FourCC::from(kind);

    DartIsolateWrapper::new(port).spawn_result_json(async move {
        let veilid_api = get_veilid_api().await?;
        let crypto = veilid_api.crypto()?;
        let csv = crypto.get(kind).ok_or_else(|| veilid_core::VeilidAPIError::invalid_argument("crypto_generate_key_pair", "kind", kind.to_string()))?;
        let out = csv.generate_keypair();
        APIResult::Ok(out)
    });
}

/// Not implemented.
#[no_mangle]
pub extern "C" fn veilid_crypto_generate_hash(port: i64, kind: u32, data: FfiStr) {
    let kind: veilid_core::CryptoKind = veilid_core::FourCC::from(kind);

    let data: Vec<u8> = data_encoding::BASE64URL_NOPAD
    .decode(
        data.into_opt_string()
            .unwrap()
            .as_bytes(),
    )
    .unwrap();

    DartIsolateWrapper::new(port).spawn_result_json(async move {
        let veilid_api = get_veilid_api().await?;
        let crypto = veilid_api.crypto()?;
        let csv = crypto.get(kind).ok_or_else(|| veilid_core::VeilidAPIError::invalid_argument("crypto_generate_hash", "kind", kind.to_string()))?;
        let out = csv.generate_hash(&data);
        APIResult::Ok(out)
    });
}

#[no_mangle]
pub extern "C" fn veilid_crypto_validate_key_pair(kind: u32, key: FfiStr, secret: FfiStr, out_validated: *mut bool) -> VeilidResult {
    let kind: veilid_core::CryptoKind = veilid_core::FourCC::from(kind);

    let key: veilid_core::PublicKey =
        veilid_core::deserialize_opt_json(key.into_opt_string()).unwrap();
    let secret: veilid_core::SecretKey =
        veilid_core::deserialize_opt_json(secret.into_opt_string()).unwrap();

    to_veilid_result(ffi_block_on(async move {
        let veilid_api = get_veilid_api().await?;
        let crypto = veilid_api.crypto()?;
        let csv = crypto.get(kind).ok_or_else(|| veilid_core::VeilidAPIError::invalid_argument("crypto_validate_key_pair", "kind", kind.to_string()))?;
        let out = csv.validate_keypair(&key, &secret);

        unsafe {
            *out_validated = out;
        }

        APIRESULT_VOID
    }))
}

/// Not implemented.
#[no_mangle]
pub extern "C" fn veilid_crypto_validate_hash(port: i64, kind: u32, data: FfiStr, hash: FfiStr) {
    let kind: veilid_core::CryptoKind = veilid_core::FourCC::from(kind);

    let data: Vec<u8> = data_encoding::BASE64URL_NOPAD
    .decode(
        data.into_opt_string()
            .unwrap()
            .as_bytes(),
    )
    .unwrap();

    let hash: veilid_core::HashDigest =
        veilid_core::deserialize_opt_json(hash.into_opt_string()).unwrap();

    DartIsolateWrapper::new(port).spawn_result(async move {
        let veilid_api = get_veilid_api().await?;
        let crypto = veilid_api.crypto()?;
        let csv = crypto.get(kind).ok_or_else(|| veilid_core::VeilidAPIError::invalid_argument("crypto_validate_hash", "kind", kind.to_string()))?;
        let out = csv.validate_hash(&data, &hash);
        APIResult::Ok(out)
    });
}

/// Not implemented.
#[no_mangle]
pub extern "C" fn veilid_crypto_distance(port: i64, kind: u32, key1: FfiStr, key2: FfiStr) {
    let kind: veilid_core::CryptoKind = veilid_core::FourCC::from(kind);

    let key1: veilid_core::CryptoKey =
        veilid_core::deserialize_opt_json(key1.into_opt_string()).unwrap();
    let key2: veilid_core::CryptoKey =
        veilid_core::deserialize_opt_json(key2.into_opt_string()).unwrap();

    DartIsolateWrapper::new(port).spawn_result_json(async move {
        let veilid_api = get_veilid_api().await?;
        let crypto = veilid_api.crypto()?;
        let csv = crypto.get(kind).ok_or_else(|| veilid_core::VeilidAPIError::invalid_argument("crypto_distance", "kind", kind.to_string()))?;
        let out = csv.distance(&key1, &key2);
        APIResult::Ok(out)
    });
}

/// Not implemented.
#[no_mangle]
pub extern "C" fn veilid_crypto_sign(port: i64, kind: u32, key: FfiStr, secret: FfiStr, data: FfiStr) {
    let kind: veilid_core::CryptoKind = veilid_core::FourCC::from(kind);

    let key: veilid_core::CryptoKey =
        veilid_core::deserialize_opt_json(key.into_opt_string()).unwrap();
    let secret: veilid_core::CryptoKey =
        veilid_core::deserialize_opt_json(secret.into_opt_string()).unwrap();
    let data: Vec<u8> = data_encoding::BASE64URL_NOPAD
        .decode(
            data.into_opt_string()
                .unwrap()
                .as_bytes(),
        )
        .unwrap();
    
    DartIsolateWrapper::new(port).spawn_result_json(async move {
        let veilid_api = get_veilid_api().await?;
        let crypto = veilid_api.crypto()?;
        let csv = crypto.get(kind).ok_or_else(|| veilid_core::VeilidAPIError::invalid_argument("crypto_sign", "kind", kind.to_string()))?;
        let out = csv.sign(&key, &secret, &data)?;
        APIResult::Ok(out)
    });
}

/// Not implemented.
#[no_mangle]
pub extern "C" fn veilid_crypto_verify(port: i64, kind: u32, key: FfiStr, data: FfiStr, signature: FfiStr) {
    let kind: veilid_core::CryptoKind = veilid_core::FourCC::from(kind);

    let key: veilid_core::CryptoKey =
        veilid_core::deserialize_opt_json(key.into_opt_string()).unwrap();
    let data: Vec<u8> = data_encoding::BASE64URL_NOPAD
        .decode(
            data.into_opt_string()
                .unwrap()
                .as_bytes(),
        )
        .unwrap();
    let signature: veilid_core::Signature =
        veilid_core::deserialize_opt_json(signature.into_opt_string()).unwrap();
    
    DartIsolateWrapper::new(port).spawn_result(async move {
        let veilid_api = get_veilid_api().await?;
        let crypto = veilid_api.crypto()?;
        let csv = crypto.get(kind).ok_or_else(|| veilid_core::VeilidAPIError::invalid_argument("crypto_verify", "kind", kind.to_string()))?;
        csv.verify(&key, &data, &signature)?;
        APIRESULT_VOID
    });
}

/// Not implemented.
#[no_mangle]
pub extern "C" fn veilid_crypto_aead_overhead(port: i64, kind: u32) {
    let kind: veilid_core::CryptoKind = veilid_core::FourCC::from(kind);
    
    DartIsolateWrapper::new(port).spawn_result(async move {
        let veilid_api = get_veilid_api().await?;
        let crypto = veilid_api.crypto()?;
        let csv = crypto.get(kind).ok_or_else(|| veilid_core::VeilidAPIError::invalid_argument("crypto_aead_overhead", "kind", kind.to_string()))?;
        let out = csv.aead_overhead();
        APIResult::Ok(out)
    });
}

/// Not implemented.
#[no_mangle]
pub extern "C" fn veilid_crypto_decrypt_aead(port: i64, kind: u32, body: FfiStr, nonce: FfiStr, shared_secret: FfiStr, associated_data: FfiStr) {
    let kind: veilid_core::CryptoKind = veilid_core::FourCC::from(kind);
    
    let body: Vec<u8> = data_encoding::BASE64URL_NOPAD
        .decode(
            body.into_opt_string()
                .unwrap()
                .as_bytes(),
        )
        .unwrap();
    
    let nonce: veilid_core::Nonce =
        veilid_core::deserialize_opt_json(nonce.into_opt_string()).unwrap();
    
    let shared_secret: veilid_core::SharedSecret =
        veilid_core::deserialize_opt_json(shared_secret.into_opt_string()).unwrap();

    let associated_data: Option<Vec<u8>> = associated_data.into_opt_string().map(|s| data_encoding::BASE64URL_NOPAD.decode(s.as_bytes()).unwrap());

    DartIsolateWrapper::new(port).spawn_result(async move {
        let veilid_api = get_veilid_api().await?;
        let crypto = veilid_api.crypto()?;
        let csv = crypto.get(kind).ok_or_else(|| veilid_core::VeilidAPIError::invalid_argument("crypto_decrypt_aead", "kind", kind.to_string()))?;
        let out = csv.decrypt_aead(&body, &nonce, &shared_secret, match &associated_data {
            Some(ad) => Some(ad.as_slice()),
            None => None
        })?;
        let out = data_encoding::BASE64URL_NOPAD.encode(&out);
        APIResult::Ok(out)
    });
}

/// Not implemented.
#[no_mangle]
pub extern "C" fn veilid_crypto_encrypt_aead(port: i64, kind: u32, body: FfiStr, nonce: FfiStr, shared_secret: FfiStr, associated_data: FfiStr) {
    let kind: veilid_core::CryptoKind = veilid_core::FourCC::from(kind);
    
    let body: Vec<u8> = data_encoding::BASE64URL_NOPAD
        .decode(
            body.into_opt_string()
                .unwrap()
                .as_bytes(),
        )
        .unwrap();
    
    let nonce: veilid_core::Nonce =
        veilid_core::deserialize_opt_json(nonce.into_opt_string()).unwrap();
    
    let shared_secret: veilid_core::SharedSecret =
        veilid_core::deserialize_opt_json(shared_secret.into_opt_string()).unwrap();

    let associated_data: Option<Vec<u8>> = associated_data.into_opt_string().map(|s| data_encoding::BASE64URL_NOPAD.decode(s.as_bytes()).unwrap());

    DartIsolateWrapper::new(port).spawn_result(async move {
        let veilid_api = get_veilid_api().await?;
        let crypto = veilid_api.crypto()?;
        let csv = crypto.get(kind).ok_or_else(|| veilid_core::VeilidAPIError::invalid_argument("crypto_encrypt_aead", "kind", kind.to_string()))?;
        let out = csv.encrypt_aead(&body, &nonce, &shared_secret, match &associated_data {
            Some(ad) => Some(ad.as_slice()),
            None => None
        })?;
        let out = data_encoding::BASE64URL_NOPAD.encode(&out);
        APIResult::Ok(out)
    });
}

/// Not implemented.
#[no_mangle]
pub extern "C" fn veilid_crypto_crypt_no_auth(port: i64, kind: u32, body: FfiStr, nonce: FfiStr, shared_secret: FfiStr) {
    let kind: veilid_core::CryptoKind = veilid_core::FourCC::from(kind);
    
    let mut body: Vec<u8> = data_encoding::BASE64URL_NOPAD
        .decode(
            body.into_opt_string()
                .unwrap()
                .as_bytes(),
        )
        .unwrap();
    
    let nonce: veilid_core::Nonce =
        veilid_core::deserialize_opt_json(nonce.into_opt_string()).unwrap();
    
    let shared_secret: veilid_core::SharedSecret =
        veilid_core::deserialize_opt_json(shared_secret.into_opt_string()).unwrap();

    DartIsolateWrapper::new(port).spawn_result(async move {
        let veilid_api = get_veilid_api().await?;
        let crypto = veilid_api.crypto()?;
        let csv = crypto.get(kind).ok_or_else(|| veilid_core::VeilidAPIError::invalid_argument("crypto_crypt_no_auth", "kind", kind.to_string()))?;
        csv.crypt_in_place_no_auth(&mut body, &nonce, &shared_secret);
        let body = data_encoding::BASE64URL_NOPAD.encode(&body);
        APIResult::Ok(body)
    });
}

#[no_mangle]
pub extern "C" fn veilid_now() -> u64 {
    veilid_core::get_aligned_timestamp().as_u64()
}

#[no_mangle]
pub extern "C" fn veilid_debug(command: FfiStr, out_result: *mut *const c_char) -> VeilidResult {
    let command = command.into_opt_string().unwrap_or_default();
    to_veilid_result(ffi_block_on(async move {
        let veilid_api = get_veilid_api().await?;
        let out = veilid_api.debug(command).await?;

        unsafe {
            *out_result = out.into_ffi_value();
        }

        APIRESULT_VOID
    }))
}

#[no_mangle]
pub extern "C" fn veilid_version_string() -> *mut c_char {
    veilid_core::veilid_version_string().into_ffi_value()
}

#[repr(C)]
pub struct VeilidVersion {
    pub major: u32,
    pub minor: u32,
    pub patch: u32,
}

#[no_mangle]
pub extern "C" fn veilid_version() -> VeilidVersion {
    let (major, minor, patch) = veilid_core::veilid_version();
    VeilidVersion {
        major,
        minor,
        patch,
    }
}
